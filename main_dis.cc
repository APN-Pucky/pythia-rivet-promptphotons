// main36.cc is a part of the PYTHIA event generator.
// Copyright (C) 2023 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords:
//            Basic usage
//            DIS

// Basic setup for Deeply Inelastic Scattering at HERA.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/Pythia8Rivet.h"
#include "Rivet/AnalysisHandler.hh"
#include "Pythia8Plugins/HepMC3.h"

using namespace Pythia8;

int main(int argc, char * argv[]) {
    // Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << " Unexpected number of command-line arguments. \n You are"
         << " expected to provide one input and one output file name. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Check that the provided input name corresponds to an existing file.
  ifstream is(argv[1]);
  if (!is) {
    cerr << " Command-line file " << argv[1] << " was not found. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Beam energies, minimal Q2, number of events to generate.
  double eProton   = 820.;
  double eElectron = 27.5;
  double Q2min     = 3.;
  int    nEvent    = 100000;

  // Generator. Shorthand for event.
  Pythia pythia;
  Event& event = pythia.event;

  // Set up incoming beams, for frame with unequal beam energies.
  pythia.readString("Beams:frameType = 2");
  // BeamA = proton.
  pythia.readString("Beams:idA = 2212");
  pythia.settings.parm("Beams:eA", eProton);
  // BeamB = electron.
  pythia.readString("Beams:idB = -11");
  pythia.settings.parm("Beams:eB", eElectron);

  // Set up DIS process within some phase space.
  // Neutral current (with gamma/Z interference).
  pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");
  // Uncomment to allow charged current.
  //pythia.readString("WeakBosonExchange:ff2ff(t:W) = on");
  // Phase-space cut: minimal Q2 of process.
  pythia.settings.parm("PhaseSpace:Q2Min", Q2min);

  // Set dipole recoil on. Necessary for DIS + shower.
  pythia.readString("SpaceShower:dipoleRecoil = on");

  // Allow emissions up to the kinematical limit,
  // since rate known to match well to matrix elements everywhere.
  pythia.readString("SpaceShower:pTmaxMatch = 2");

  // QED radiation off lepton not handled yet by the new procedure.
  pythia.readString("PDF:lepton = off");
  pythia.readString("TimeShower:QEDshowerByL = off");



  Pythia8Rivet rivet(pythia, argv[2]);
  rivet.ignoreBeams(true);
  //rivet.addAnalysis("MC_DIS");
  //rivet.addAnalysis("MC_DIS_Check");
  rivet.addAnalysis("H1_1996_I424463");
  rivet.addAnalysis("H1_2000_S4129130");

  // Initialize.
  pythia.init();

  // Begin event loop.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;

    rivet();

  // End of event loop. Statistics and histograms.
  }
  rivet.done();
  pythia.stat();

  // Done.
  return 0;
}

