
#DEBUG=-O0 -g -ggdb

main_dis.o: main_dis.cc
	$(CXX) $^ $(shell pythia8-config --cxxflags --libs) $(shell rivet-config --libs --plugins --cppflags) -std=c++17 -o $@ $(DEBUG)

main_prompt_photon.o: main_prompt_photon.cc
	$(CXX) $^ $(shell pythia8-config --cxxflags --libs) $(shell rivet-config --libs --plugins --cppflags) -std=c++17 -o $@ $(DEBUG)

main_kroll_wada.o: main_kroll_wada.cc
	$(CXX) $^ $(shell pythia8-config --cxxflags --libs) $(shell rivet-config --libs --plugins --cppflags) -std=c++17 -o $@ $(DEBUG)

all: main_dis.o main_prompt_photon.o

clean:
	rm *.o
