// main_kroll_wada.cc

//#define LHE 1
//#define HEPMC 1
#define RIVET 1

//#define SIMPLE  // simple 
#define VINCIA // VINCIA
//#define DIRE // DIRE

//#define SINGLE // veto everything but first emission

#include "Pythia8/Pythia.h"

#ifdef RIVET
#include "Pythia8Plugins/Pythia8Rivet.h"
#include "Rivet/AnalysisHandler.hh"
#endif

#ifdef HEPMC
#include "Pythia8Plugins/HepMC3.h"
#endif

#include <iostream>
#include <chrono>  // For timing

using namespace Pythia8;

class MyUserHooks : public UserHooks {
  // Allow a veto after (by default) first step.
  bool canVetoFSREmission() {
    //std::cout << "FSR veto" << std::endl;
    //exit(-1);
	  return true;
  }
  // Access the event in the interleaved evolution after first step.
  bool  doVetoFSREmission( int sizeOld, const Event& event, int iSys, bool inResonance = false)   {
    if (sizeOld > 7) {
      //event.list();
      //std::cout << "FSR " << sizeOld << " vetoed" << std::endl;
      return true;
    }
    return false;
    //std::cout << "FSR " << sizeOld << std::endl;
    //exit(-1);
    // Not intending to veto any events here.
    //return sizeOld > 7;
  }
};

int main(int argc, char* argv[]) {
  int totalIterations = 10000;
  std::string filename = "event";

  if (argc > 1){
	filename = argv[1]; 
  }
  if (argc > 2){
	totalIterations = atoi(argv[2]);
  }



  // Generator.
  Pythia pythia;

  // Set up to do a user veto and send it in.
#ifdef SINGLE
  auto myUserHooks = make_shared<MyUserHooks>();
  pythia.setUserHooksPtr( myUserHooks );
#endif


  //pythia.readString("PDF:pSet = LHAPDF6:NNPDF40_nlo_as_01180/0");
  pythia.readString("Check:event = off");

  // On shell prompt photon
  //pythia.readString("PromptPhoton:all = on");
  pythia.readString("PromptPhoton:qg2qgamma = on");
  pythia.readString("PromptPhoton:qqbar2ggamma = on");

  // Kroll-Wada
  pythia.readString("Enhancements:doEnhance = on");
  pythia.readString("EnhancedSplittings:List = { isr:A2AQ=1.,isr:Q2QA=1.,fsr:Q2QA=1.,fsr:A2LL=100.0 }");
  // disable other lepton sources
  pythia.readString("HadronLevel:all = off");
  pythia.readString("HadronLevel:Decay = off");
  pythia.readString("PartonLevel:MPI = off");
  pythia.readString("PartonLevel:ISR = off");
  pythia.readString("PartonLevel:FSR = on");
  pythia.readString("PartonLevel:Remnants = off");


#ifdef SIMPLE
  pythia.readString("PartonShowers:model  = 1");
  // disable QCD shower (=> more QED shower?)
  pythia.readString("TimeShower:QCDshower=off");
  pythia.readString("SpaceShower:QCDshower=off");

  // only photon -> e
  pythia.readString("TimeShower:nGammaToLepton = 1");
  pythia.readString("TimeShower:nGammaToQuark = 0");
#endif
#ifdef DIRE
  pythia.readString("PartonShowers:model=3");
  //pythia.readString("DireSpace:nFinalMax = 0");
  //pythia.readString("DireTimes:nFinalMax = 8");

  pythia.readString("TimeShower:QEDshowerByL=on");
  pythia.readString("TimeShower:QEDshowerByGamma=on");

  pythia.readString("TimeShower:QCDshower=off");
  pythia.readString("TimeShower:QCDshower=off");
  pythia.readString("SpaceShower:QCDshower=off");
  //pythia.readString("Enhance:Dire_fsr_qed_22->11&11a = 1000");
  //pythia.readString("Enhance:Dire_fsr_qed_22->11&11b = 1000");
#endif
#ifdef VINCIA
  pythia.readString("PartonShowers:model  = 2");
  pythia.readString("Vincia:nGammaToLepton = 1");
  pythia.readString("Vincia:nGammaToQuark = 0");
  pythia.readString("Vincia:mMaxGamma = 10."); //10.0 default
  //disable QCD
  pythia.readString("Vincia:nGluonToQuark = 0");
  pythia.readString("Vincia:convertGluonToQuark = off");
  pythia.readString("Vincia:convertQuarkToGluon = off");
#endif


  // Switch off generation of steps subsequent to the process level one.
  // (These will not be stored anyway, so only steal time.)
  //pythia.readString("PartonLevel:all = off");


  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 0");

  pythia.readString("PhaseSpace:pTHatMin = 0");
  pythia.readString("PhaseSpace:pTHatMax = 10");


  // LHC 14 TeV initialization.
  pythia.readString("Beams:eCM = 5020.");

#ifdef LHE
  // Create an LHAup object that can access relevant information in pythia.
  LHAupFromPYTHIA8 myLHA(&pythia.process, &pythia.info);

  // Open a file on which LHEF events should be stored, and write header.
  myLHA.openLHEF(filename + ".lhe");

  // Store initialization info in the LHAup object.
  myLHA.setInit();

  // Write out this initialization info on the file.
  myLHA.initLHEF();
#endif


  /*
  Rivet::AnalysisHandler *rivet = new Rivet::AnalysisHandler;
  rivet->addAnalysis("ALICE_2022_FOCAL");
  HepMC::Pythia8ToHepMC toHepMC;
  // */

#ifdef RIVET
  //*
  Pythia8Rivet rivet(pythia, filename + ".yoda");
  rivet.ignoreBeams(true);
  //rivet.addAnalysis("ALICE_2022_FOCAL:NOTPOWHEG=TRUE");
  //rivet.addAnalysis("ATLAS_2017_I1645627");
  //rivet.addAnalysis("ALICE_2020_I1797621");
  rivet.addAnalysis("MC_KROLL_WADA");
  rivet.addAnalysis("MC_FSPARTICLES");
  rivet.addAnalysis("MC_DILEPTON");
  //rivet.addAnalysis("ALICE_2022_FLORIAN:ISOLATION=FALSE");
  //*/
#endif


#ifdef HEPMC
  ///*
  // Interface for conversion from Pythia8::Event to HepMC event.
  Pythia8ToHepMC toHepMC(filename + ".hepmc");
  //*/
#endif

   // Extract settings to be used in the main program.
  int    nEvent    = pythia.mode("Main:numberOfEvents");
  int    nAbort    = pythia.mode("Main:timesAllowErrors");

  // Initialization.
  pythia.init();

  // Begin event loop.
  int iAbort = 0;


  // Get the start time of the entire loop
  auto start = std::chrono::high_resolution_clock::now();

   

  // Loop over events.
  for (int i = 0; i < totalIterations; ++i) {

	          // Get the start time for this iteration
        auto iterStart = std::chrono::high_resolution_clock::now();

        // Simulate some work (optional)
        // std::this_thread::sleep_for(std::chrono::milliseconds(1));

        // Get the current time
        auto now = std::chrono::high_resolution_clock::now();

        // Calculate the elapsed time since the loop started
        std::chrono::duration<double> elapsed = now - start;

        // Calculate the average time per iteration so far
        double averageTimePerIter = elapsed.count() / (i + 1);  // in seconds

        // Estimate time to finish (eta), in seconds
        double eta = averageTimePerIter * (totalIterations - (i + 1));

        // Get the time taken for this iteration
        std::chrono::duration<double, std::micro> iterElapsed = now - iterStart;

        // Print the current iteration, time taken for the iteration, and ETA
        //std::cout << "Event " << i << ": time taken = " << iterElapsed.count() << " microseconds"
        //          << ", ETA = " << eta << " seconds" << std::endl;

    // Generate event.
    if (!pythia.next()) {

      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) {
        cout << " Aborted since reached end of Les Houches Event File\n";
        break;
      }

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }
#ifdef RIVET
    ///*
    //cout << "pre rivet" << std::endl;
    rivet();
    //cout << "post rivet" << std::endl;
    //*/
#endif

#ifdef LHE
    // Store event info in the LHAup object.
    myLHA.setEvent();

    // Write out this event info on the file.
    // With optional argument (verbose =) false the file is smaller.
    myLHA.eventLHEF();
#endif
    
#ifdef HEPMC
    toHepMC.writeNextEvent( pythia );
    // */
#endif
  }
#ifdef RIVET
  rivet.done();
#endif

  // Statistics: full printout.
  pythia.stat();

#ifdef LHE
  // Update the cross section info based on Monte Carlo integration during run.
  myLHA.updateSigma();

  // Write endtag. Overwrite initialization info with new cross sections.
  myLHA.closeLHEF(true);
#endif

  // Done.
  return 0;
}


