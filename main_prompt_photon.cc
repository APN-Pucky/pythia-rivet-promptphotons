// main20.cc is a part of the PYTHIA event generator.
// Copyright (C) 2020 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program. It shows how PYTHIA 8 can write
// a Les Houches Event File based on its process-level events.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/Pythia8Rivet.h"
#include "Rivet/AnalysisHandler.hh"
#include "Pythia8Plugins/HepMC3.h"

using namespace Pythia8;
double CorrectPhiDelta(double angle1, double angle2)
{
  double pi = M_PI;
  double phi = abs(angle1 - angle2);
  if (phi >= pi)
    return 2 * pi - phi;
  else
    return phi;
}
int main(int argc, char* argv[]) {
    // Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << " Unexpected number of command-line arguments. \n You are"
         << " expected to provide one input and one output file name. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Check that the provided input name corresponds to an existing file.
  ifstream is(argv[1]);
  if (!is) {
    cerr << " Command-line file " << argv[1] << " was not found. \n"
         << " Program stopped! " << endl;
    return 1;
  }


  // Generator.
  Pythia pythia;

  pythia.readString("PDF:pSet = LHAPDF6:NNPDF40_nlo_as_01180/0");

  // Process selection. Minimal masses for gamma*/Z and W+-.
  //pythia.readString("PromptPhoton:all = on");
  pythia.readString("PromptPhoton:qg2qgamma = on");
  pythia.readString("PromptPhoton:qqbar2ggamma = on");

  // Switch off generation of steps subsequent to the process level one.
  // (These will not be stored anyway, so only steal time.)
  //pythia.readString("PartonLevel:all = off");
      pythia.readString("Check:event = off");
      pythia.readString("PartonLevel:MPI = off");
      pythia.readString("PartonLevel:ISR = off");
      pythia.readString("PartonLevel:FSR = off");
      pythia.readString("PartonLevel:Remnants = off");
      pythia.readString("HadronLevel:all = off");

      pythia.readString("Random:setSeed = on");
      pythia.readString("Random:seed = 0");

      pythia.readString("PhaseSpace:pTHatMin = 5");
      pythia.readString("PhaseSpace:pTHatMax = 80");


  // Create an LHAup object that can access relevant information in pythia.
  //LHAupFromPYTHIA8 myLHA(&pythia.process, &pythia.info);

  // Open a file on which LHEF events should be stored, and write header.
  //myLHA.openLHEF("promptphoton.lhe");

  // LHC 14 TeV initialization.
  pythia.readString("Beams:eCM = 8000.");

  // Store initialization info in the LHAup object.
  //myLHA.setInit();

  // Write out this initialization info on the file.
  //myLHA.initLHEF();


  /*
  Rivet::AnalysisHandler *rivet = new Rivet::AnalysisHandler;
  rivet->addAnalysis("ALICE_2022_FOCAL");
  HepMC::Pythia8ToHepMC toHepMC;
  // */

  //*
  Pythia8Rivet rivet(pythia, argv[2]);
  rivet.ignoreBeams(true);
  //rivet.addAnalysis("ALICE_2022_FOCAL:NOTPOWHEG=TRUE");
  //rivet.addAnalysis("ATLAS_2017_I1645627");
  rivet.addAnalysis("ALICE_2022_FLORIAN:ISOLATION=TRUE");
  rivet.addAnalysis("ALICE_2022_FLORIAN:ISOLATION=FALSE");
  //*/


  /*
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);
  */

   // Extract settings to be used in the main program.
  int    nEvent    = pythia.mode("Main:numberOfEvents");
  int    nAbort    = pythia.mode("Main:timesAllowErrors");

  // Initialization.
  pythia.init();

  // Begin event loop.
  int iAbort = 0;
   

  // Loop over events.
  for (int i = 0; i < 1e6; ++i) {

    // Generate event.
    if (!pythia.next()) {

      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) {
        cout << " Aborted since reached end of Les Houches Event File\n";
        break;
      }

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }
	//if(pythia.event[5].isFinal() && pythia.event[5].isVisible())
	//if(pythia.event[6].isFinal() && pythia.event[6].isVisible())
	//cout << CorrectPhiDelta(pythia.event[5].phi(), pythia.event[6].phi()) << endl;


    /*
    HepMC::GenEvent *hepmcevt = new HepMC::GenEvent();
    toHepMC.fill_next_event(pythia, hepmcevt);

    rivet->analyze(*hepmcevt);
    delete hepmcevt ;
    */
    ///*
    rivet();
    //*/

    // Store event info in the LHAup object.
    //myLHA.setEvent();

    // Write out this event info on the file.
    // With optional argument (verbose =) false the file is smaller.
    //myLHA.eventLHEF();
    
    /*
    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build, but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    hepmcevt->set_beam_particles(0,0);
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;
    // */
  }
  rivet.done();

  // Statistics: full printout.
  pythia.stat();

  // Update the cross section info based on Monte Carlo integration during run.
  //myLHA.updateSigma();

  // Write endtag. Overwrite initialization info with new cross sections.
  //myLHA.closeLHEF(true);

  // Done.
  return 0;
}


